import assert from 'assert'
import * as StringUtils from './utils';

describe('Utils', function () {
  it('convert the name properly', () => {
    assert(StringUtils.formatName('name'), 'Name');
    assert(StringUtils.formatName('name_name'), 'Name name');
  });


  it('find by id', () => {
    const data = [{id: 1}, {id: 2}, {id: 3}];
    assert(StringUtils.findMatchingById(1), {id: 1});
    assert(StringUtils.findMatchingById(4), null);
  });
});
