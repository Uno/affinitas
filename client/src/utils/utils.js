export const formatName = name => {
  return name.charAt(0).toUpperCase() + name.slice(1).replace('_', ' ')
};

export const findMatchingById = (id, arr) => {
  if (!arr) return {};

  return arr.find((ans) => {
    return ans.id === id;
  });
};
