import React, { Component } from 'react';
import './App.css';
import CategoryList from './components/CategoryList/CategoryList';

class App extends Component {

  render() {
    return (
      <div className="App">
        <div className="Explanation">
          Every answer is saved as soon as you choose it.
        </div>
        <CategoryList />
      </div>
    );
  }
}

export default App;
