import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import './CategoryList.css';

import { addQuestions } from "../../reducers/questions";
import { addCategories } from "../../reducers/categories";
import { addAnswers } from "../../reducers/answers";

import { formatName } from "../../utils/utils";

import Category from './Category/Category'
import QuestionList from '../QuestionList/QuestionList';

class CategoryList extends React.Component {
  componentDidMount() {
    fetch('/questions')
      .then(res => res.json())
      .then(questions => this.props.addQuestions({ questions }))
      .then(() => fetch('/questions/categories'))
      .then(res => res.json())
      .then(categories => this.props.addCategories({ categories }))
      .then(() => fetch('/questions/answers'))
      .then(res => res.json())
      .then(answers => this.props.addAnswers({ answers }))
  }

  render() {
    // Show loading until we're ready to show the content
    if (this.props.categories.length === 0) {
      return <div className="CategoryListContainer">Loading...</div>;
    }
    return (
      <div className="CategoryListContainer">
        {this.props.categories.map((c) => (
          <Category key={c.id} name={formatName(c.name)} id={c.id}>
            <QuestionList questions={this.props.questions} answers={this.props.answers} category={c.name} />
          </Category>))}
      </div>
    );
  }
}

CategoryList.propTypes = {
  categories: PropTypes.array,
  questions: PropTypes.array,
  answers: PropTypes.array,
};

CategoryList.defaultProps = {
  questions: [],
  categories: [],
  answers: [],
};

const mapStateToProps = state => ({
  questions: state.questions.questions,
  categories: state.categories.categories,
  answers: state.answers.answers,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  addQuestions,
  addCategories,
  addAnswers,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CategoryList);

