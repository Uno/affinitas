import React from 'react';
import PropTypes from 'prop-types';
import './Category.css';

class Category extends React.Component {
  render() {
    return (
      <div className="Category" key={`category${this.props.id}`}>
        <div className="CategoryName">
          {this.props.name}
        </div>
        <div className="QuestionList">
          {this.props.children}
        </div>
      </div>
    );
  }
}

Category.propTypes = {
  name: PropTypes.string.isRequired,
  id: PropTypes.number.isRequired,
  children: PropTypes.object,
};

export default Category;
