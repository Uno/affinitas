import React from 'react';
import PropTypes from 'prop-types';
import '../Question/Question.css';

import AnswerNumber from '../AnswerNumber/AnswerNumber';

class SubQuestion extends React.Component {
  render() {
    const q = this.props.question;
    return (
      <div>
        <div className="QuestionText">{q.question}</div>
        <AnswerNumber
          id={q.id}
          parentId={q.parentId}
          range={q.question_type.range}
          answer={this.props.answer}
        />
      </div>
    );
  }
}

SubQuestion.propTypes = {
  answer: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  question: PropTypes.object,
};
SubQuestion.defaultProps = {};

export default SubQuestion;