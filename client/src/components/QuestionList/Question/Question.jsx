import React from 'react';
import PropTypes from 'prop-types';
import './Question.css';

import Answer from '../Answer/Answer';

class Question extends React.Component {
  render() {
    const q = this.props.question;
    return (
      <div>
        <div className="QuestionText">{q.question}</div>
        <Answer
          id={q.id}
          options={q.question_type.options}
          type={q.question_type.type}
          answer={this.props.answer || ""}
          condition={q.question_type.condition}
        />
      </div>
    );
  }
}

Question.propTypes = {
  question: PropTypes.object.isRequired,
  answer: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
};
Question.defaultProps = {};

export default Question;