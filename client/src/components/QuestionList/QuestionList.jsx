import React from 'react';
import PropTypes from 'prop-types';

import { findMatchingById } from "../../utils/utils";

import Question from './Question/Question';

class QuestionList extends React.Component {
  renderQuestion(obj) {
    return (
      <Question
        key={obj.id}
        question={obj}
        answer={findMatchingById(obj.id, this.props.answers)}
      />
    );
  }

  render() {
    return (
      <div>
        {this.props.questions.map((obj) => {
          return obj.category === this.props.category ? this.renderQuestion(obj) : null;
        })}
      </div>
    );
  }
}

QuestionList.propTypes = {
  category: PropTypes.string.isRequired,
  questions: PropTypes.array.isRequired,
  answers: PropTypes.array.isRequired,
};
QuestionList.defaultProps = {};

export default QuestionList;