import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import './Answer.css';

import { updateAnswer } from "../../../reducers/answers";
import SubQuestion from '../SubQuestion/SubQuestion';

class Answer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: "",
    };
    this.onChange = this.onChange.bind(this);
    this.renderConditional = this.renderConditional.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ value: nextProps.answer.value || ""});
  }


  onChange(e) {
    const data = {
      id: this.props.id,
      value: e.target.value,
    };
    this.props.updateAnswer(data);
    this.setState({value: e.target.value});
    fetch('/questions/answer', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  }

  renderConditional() {
    return (
      <SubQuestion
        answer={this.props.answer.sub}
        question={Object.assign(
        {},
        this.props.condition.if_positive,
        {
          parentId: this.props.id,
          id: `sub-${this.props.id}`
        }
      )}/>
    );
  }

  render() {
    const conditionApply = this.props.type === 'single_choice_conditional' && this.state.value === this.props.condition.predicate.exactEquals[1];
    return (
      <div>
        <select className="SelectComponent" onChange={this.onChange} id={this.props.id} value={this.state.value}>
          <option value="" disabled hidden>Choose here</option>
          {this.props.options.map((o) => (<option key={o} value={o}>{o}</option>))}
        </select>
        {conditionApply ? this.renderConditional() : null}
      </div>
    );
  }
}

Answer.propTypes = {
  type: PropTypes.string.isRequired,
  options: PropTypes.array.isRequired,
  id: PropTypes.number.isRequired,
  answer: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  condition: PropTypes.object,
};

Answer.defaultProps = {};

const mapDispatchToProps = dispatch => bindActionCreators({
  updateAnswer,
}, dispatch);

export default connect(
  null,
  mapDispatchToProps
)(Answer);