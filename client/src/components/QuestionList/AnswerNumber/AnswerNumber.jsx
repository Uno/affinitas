import React from 'react';
import PropTypes from 'prop-types';
import Slider from 'react-rangeslider';
import 'react-rangeslider/lib/index.css';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import '../Answer/Answer.css';

import { addSubAnswer } from "../../../reducers/answers";

class AnswerNumber extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: (this.props.answer && this.props.answer.value) || this.props.range.from,
    };
    this.onChange = this.onChange.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ value: nextProps.answer.value });
  }

  onChange() {
    const data = {
      parentId: this.props.parentId,
      id: this.props.id,
      value: this.state.value,
    };
    this.props.addSubAnswer(data);
    fetch('/questions/answer/sub', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  }

  render() {
    return (
      <div>
        <Slider
          min={this.props.range.from}
          max={this.props.range.to}
          value={this.state.value}
          orientation="horizontal"
          onChange={(value) => this.setState({ value })}
          onChangeComplete={this.onChange}
        />
        <span>{this.state.value}</span>
      </div>
    );
  }
}

AnswerNumber.propTypes = {
  id: PropTypes.string.isRequired,
  parentId: PropTypes.number.isRequired,
  range: PropTypes.object.isRequired,
  answer: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
};
AnswerNumber.defaultProps = {};


const mapDispatchToProps = dispatch => bindActionCreators({
  addSubAnswer,
}, dispatch);

export default connect(
  null,
  mapDispatchToProps
)(AnswerNumber);