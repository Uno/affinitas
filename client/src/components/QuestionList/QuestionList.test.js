import React from 'react';
import ReactShallowRenderer from 'react-test-renderer/shallow';
import QuestionList from './QuestionList';

it('renders QuestionList', () => {
  const questions = [{
    category: 'hard_fact',
    id: 0,
    question: 'What is your gender?',
    question_type: {
      options: ['male', 'female'],
      type: 'single_choice'
    }
  }];
  const category = 'hard_fact';
  const answers = [{}]
  const renderer = new ReactShallowRenderer();
  renderer.render(<QuestionList questions={questions} category={category} answers={answers}/>);

  const result = renderer.getRenderOutput();
  expect(result.type).toBe('div');
});
