import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux'
import questions from './questions';
import categories from './categories';
import answers from './answers';

export default combineReducers({
  routing: routerReducer,
  questions,
  categories,
  answers,
})