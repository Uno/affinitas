export const ADD_QUESTIONS = 'questions/ADD_QUESTIONS';
const initialState = {
  questions: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_QUESTIONS:
      return action.questions;
    default:
      return state
  }
}

export const addQuestions = (q) => {
  return dispatch => {
    dispatch({
      type: ADD_QUESTIONS,
      questions: q,
    });
  };
};