export const ADD_CATEGORIES = 'categories/ADD_CATEGORIES';
const initialState = {
  categories: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_CATEGORIES:
      return action.categories;
    default:
      return state
  }
}

export const addCategories = (c) => {
  return dispatch => {
    dispatch({
      type: ADD_CATEGORIES,
      categories: c,
    });
  };
};