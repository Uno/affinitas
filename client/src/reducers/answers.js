export const ADD_ANSWERS = 'answers/ADD_ANSWERS';
export const UPDATE_ANSWERS = 'answers/UPDATE_ANSWERS';
export const ADD_SUB_ANSWER = 'answers/ADD_SUB_ANSWERS';
const initialState = {
  answers: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_ANSWERS:
      return action.answers;
    case UPDATE_ANSWERS: {
      const id = state.answers.findIndex(x => x.id === action.answer.id);
      state[id] = action.answer;
      return state;
    }
    case ADD_SUB_ANSWER: {
      const id = state.answers.findIndex(x => x.id === action.answer.parentId);
      state.answers[id].sub = action.answer;
      return state;
    }
    default:
      return state
  }
}

export const addAnswers = (answers) => {
  return dispatch => {
    dispatch({
      type: ADD_ANSWERS,
      answers,
    });
  };
};

export const updateAnswer = (a) => {
  return dispatch => {
    dispatch({
      type: UPDATE_ANSWERS,
      answer: a,
    });
  };
};


export const addSubAnswer = (a) => {
  return dispatch => {
    dispatch({
      type: ADD_SUB_ANSWER,
      answer: a,
    });
  };
};