import React from 'react';
import ReactShallowRenderer from 'react-test-renderer/shallow';
import App from './App';
import CategoryList from './components/CategoryList/CategoryList';

it('renders without crashing', () => {
  const renderer = new ReactShallowRenderer();
  renderer.render(<App />);

  const result = renderer.getRenderOutput();
  expect(result.type).toBe('div');
  expect(result.props.children).toEqual([
    <div className="Explanation">Every answer is saved as soon as you choose it.</div>,
    <CategoryList />
  ]);
});
