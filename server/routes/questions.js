var express = require('express');
var router = express.Router();
var db = require('../database/db');

router.get('/', function(req, res, next) {
  db.loadDatabase({}, function(){
    res.send(db.getCollection('questions').data);
  });
});

router.get('/categories', function(req, res, next) {
  db.loadDatabase({}, function(){
    res.send(db.getCollection('categories').data);
  });
});

router.get('/category/:id', function(req, res, next) {
  db.loadDatabase({}, function(){
    var questions = db.getCollection('questions');
    res.send(questions.find({category: req.params.id}));
  });
});

router.get('/answer/:id', function(req, res, next) {
  db.loadDatabase({}, function(){
    var answers = db.getCollection('answers');
    var answer = answers.find({id: req.params.id});
    if(answer.length === 0) {
      res.sendStatus(404);
    } else {
      res.send(answer);
    }
  });
});

router.get('/answers', function(req, res, next) {
  db.loadDatabase({}, function(){
    res.send(db.getCollection('answers').data);
  });
});

router.post('/answer', function(req, res, next) {
  db.loadDatabase({}, function(){
    var answers = db.getCollection('answers');
    var exists = answers.find({id: req.body.id});
    if(exists.length === 0) {
      answers.insert(req.body);
      db.saveDatabase();
      res.sendStatus(200);
    } else {
      exists[0].value = req.body.value;
      answers.update(exists);
      db.saveDatabase();
      res.sendStatus(200);
    }
  });
});

router.post('/answer/sub', function(req, res, next) {
  db.loadDatabase({}, function(){
    var answers = db.getCollection('answers');
    var exists = answers.find({id: req.body.parentId});
    if(exists.length === 0) {
      answers.insert(req.body);
      db.saveDatabase();
      res.sendStatus(404);
    } else {
      exists[0].sub = req.body;
      answers.update(exists);
      db.saveDatabase();
      res.sendStatus(200);
    }
  });
});

module.exports = router;
