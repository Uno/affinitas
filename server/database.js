var loki = require('lokijs');
var db = new loki('db.json');
var questions = require('./database/personality_test.json');

var questionCollection = db.addCollection('questions');
questions.questions.map(function (obj, idx) {
  questionCollection.insert(Object.assign({}, {id: idx}, obj));
});

var categoryCollection = db.addCollection('categories');
questions.categories.map(function (str, idx) {
  categoryCollection.insert({
    id: idx,
    name: str
  });
});

db.addCollection('answers');

db.saveDatabase();