var request = require('supertest');
describe('Checking endpoints', function () {
  var server;
  beforeEach(function () {
    server = require('./server')();
  });
  afterEach(function (done) {
    server.close(done);
  });
  it('404 when not defined', function (done) {
    request(server)
      .get('/foo/bar')
      .expect(404, done);
  });
  it('loads questions', function (done) {
    request(server)
      .get('/questions')
      .expect(200, done);
  });
  it('loads categories', function (done) {
    request(server)
      .get('/questions/categories')
      .expect(200, done);
  });
  it('loads categories by id', function (done) {
    request(server)
      .get('/questions/category/0')
      .expect(200, done);
  });

  it('post an answer', function (done) {
    request(server)
      .post('/questions/answer')
      .type('form')
      .send({ id: 0, value: 'yes' })
      .set('Accept', /application\/json/)
      .expect(200, done);
  });

  it('post a sub-answer', function (done) {
    request(server)
      .post('/questions/answer')
      .type('form')
      .send({ id: 0, value: 'yes' })
      .set('Accept', /application\/json/)
      .expect(200);

    request(server)
      .post('/questions/answer/sub')
      .type('form')
      .send({ parentId: 0, id: 0, value: 'yes' })
      .set('Accept', /application\/json/)
      .expect(200, done);
  });

  it('get an answer by id', function (done) {
    request(server)
      .post('/questions/answer')
      .type('form')
      .send({ id: 0, value: 'yes' })
      .set('Accept', /application\/json/)
      .expect(200);

    request(server)
      .get('/questions/answer/0')
      .expect(200, done);
  });

  it('get all answers', function (done) {
    request(server)
      .post('/questions/answer')
      .type('form')
      .send({ id: 0, value: 'yes' })
      .set('Accept', /application\/json/)
      .expect(200);

    request(server)
      .get('/questions/answers')
      .expect(200, done);
  });
});