# Personality Test

## Setup

On both `client` and `server` you need to run:

`yarn install` or `npm install`


## Client side

` npm run start ` or `yarn start`

##### Testing
` npm run test ` or `yarn test`


## Server side

#### Testing
` npm run test ` or `yarn test`

#### If on mac/linux

` npm run start ` or `yarn start`

#### If on win

` npm run start-win ` or `yarn start-win`